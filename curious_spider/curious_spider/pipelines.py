# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class CuriousSpiderPipeline(object):
    def process_item(self, item, spider):
        return item


class PipelineOne(object):
    def process_item(self, item, spider):
    	print 'Pipeline - 1'
        return item


class PipelineTwo(object):
    def process_item(self, item, spider):
    	print 'Pipeline - 2'
        return item
