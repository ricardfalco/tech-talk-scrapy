# -*- coding: utf-8-*-
import re
from twentyonebuttons_crawler.base_spider import BaseSpider
from twentyonebuttons_crawler.items import Product

class GoikoSpider(BaseSpider):
    """
    Spider to crawl Goiclothing.
    """
    name = "goiko_spider"
    allowed_domains = ["goiclothing.com"]
    base_domain = 'https://goiclothing.com'

    """

    Cambiar los enqueue_request por lo que necesite realmente

    Necesita BaseSpider

    """

    start_urls = ["https://goiclothing.com/collections/ver-todo"]

    def parse(self, response):
        elements = '//a[@class="grid-view-item__link grid-view-item__image-container"]'
        urls = response.xpath(elements).xpath('@href').extract()
        print 'Total product urls obtain: {}'.format(len(urls))
        for url in urls:
            if not url.startswith('http'):
                url = '{}{}'.format(self.base_domain, url)
            yield self.enqueue_request(
                url, callback=self.parse_item)

        next_page = '(//a[@class="btn btn--secondary btn--narrow"]/@href)[last()]'
        next_page = response.xpath(next_page).extract()
        if next_page:
            next_page = next_page[0]
            if not next_page.startswith('http'):
                next_page = '{}{}'.format(self.base_domain, next_page)
            yield self.enqueue_request(
                next_page, callback=self.parse)

    def parse_item(self, response):
        print "Parsing {}" .format(response.url)

        name = '//h1[@itemprop="name"]'
        reference = '//input[@id="pid"]'
        description = '//div[@itemprop="description"]/p/text()'
        main_image = '//ul[@class="grid grid--uniform product-single__thumbnails product-single__thumbnails-product-template"]/li/a'
        price = '//span[@class="money"]'
        sale_price = '//span[contains(@class, "sale")]//span[@class="money"]'

        product = Product()
        # Price
        price_value = self.get_price(
            response, sale_price, css=False,
            regex=u'\u20ac(.+)', search=True)
        if not price_value:
            price_value = self.get_price(
                response, price, css=False,
                regex=u'\u20ac(.+)', search=True)
        product['price'] = price_value
        product['price_currency'] = 'EUR'
        # URL
        product['url'] = response.url
        # Name
        product['name'] = self.get_name(
            response, name, css=False)
        # Reference
        product['reference'] = self.get_reference(
            response, reference, css=False, properties='@value')
        # Description
        description_value = self.get_full_description(response, description, css=False, join_option='\n')
        product['description'] = description_value
        # Images
        images = self.get_images(response, main_image, css=False, properties='@href')
        if images is not None and images != []:
            changes = ['', '']
            images = self.convert_image(images, changes, append='https:')
        else:
            main_image = '//meta[@property="og:image"]'
            images = self.get_images(response, main_image, css=False, properties='@content')
        product['image_urls'] = images[:5]
        # Titles
        product['title1'] = "Goiclothing"
        product['title2'] = product.get('name')

        yield product
